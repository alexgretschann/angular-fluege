import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { Dashboard }  from './dashboard.component';
import { FluegService } from './flueg.service';

@NgModule({
  imports: [ BrowserModule ],
  declarations: 
    [ 
      AppComponent,
      Dashboard
    ],
    providers: [
		  FluegService
	  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
