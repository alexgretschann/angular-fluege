import { Component } from '@angular/core';
import { Dashboard } from './dashboard.component';

let currentTime = new Date();

@Component({
    selector: 'my-app',
    template: `
        <h2>{{currentTime | date:"H:mm"}} Flüge</h2>
        <dashboard></dashboard>
    `,
})

export class AppComponent { 
    currentTime = currentTime.getTime();
}
