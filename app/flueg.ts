export class Flueg {
    id: number;
    destination: string;
    hour: number; 
    min: number;
}