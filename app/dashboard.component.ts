import { Component, OnInit} from '@angular/core';

import { Flueg } from './flueg';
import { FluegService } from './flueg.service';

@Component({
    selector: 'dashboard',
    templateUrl: 'app/dashboard.component.html'
})

export class Dashboard implements OnInit {
    selectedFlueg: Flueg;
    fluege: Flueg[] = [];

    constructor(
        private fluegService: FluegService)
    { }

    ngOnInit(): void {
        this.fluege = this.fluegService.getFluege();
        console.log('log> ' + JSON.stringify(this.fluege));
    }

    onClick(flueg: Flueg): void {
        this.selectedFlueg = flueg;
    }
}