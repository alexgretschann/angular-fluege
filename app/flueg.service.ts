import { Injectable } from '@angular/core';
import { Flueg } from './flueg';

let today = new Date();

export const FLUEGE: Flueg[] = [
    { id: 100, destination: 'Frankfurt', hour: today.getHours() + 1, min: today.getMinutes() +10 },
    { id: 101, destination: 'München', hour: today.getHours() + 2, min: today.getMinutes() + 10 },
    { id: 102, destination: 'Berlin', hour: today.getHours() + 3, min: today.getMinutes() +10 },
    { id: 103, destination: 'Dortmund', hour: today.getHours() + 4, min: today.getMinutes() +10 },
    { id: 104, destination: 'Hamburg', hour: today.getHours() + 5, min: today.getMinutes() +10 },
    { id: 105, destination: 'Dresden', hour: today.getHours() + 5, min: today.getMinutes() + 20 },
    { id: 106, destination: 'Hanover', hour: today.getHours() + 5, min: today.getMinutes() +10 },
    { id: 107, destination: 'Stuttgart', hour: today.getHours() + 6, min: today.getMinutes() +10 },
    { id: 102, destination: 'Wiesbaden', hour: today.getHours() + 5, min: today.getMinutes() +10 },
    { id: 102, destination: 'Köln', hour: today.getHours() + 7, min: today.getMinutes() +10 }
];

@Injectable()

export class FluegService {
    getFluege(): Flueg[] {
        return FLUEGE;
    }
}